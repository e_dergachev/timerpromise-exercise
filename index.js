const Timer = require('timerpromise');
(async () => {
    let begin = new Date();
    await ( new Timer(3) ).start;
    let firstTime = new Date();
    console.log(`${(firstTime - begin)/1000} seconds have passed`);
    await ( new Timer(2) ).start;
    let secondTime = new Date();
    console.log(`${(secondTime - firstTime)/1000} seconds have passed`);
    console.log(`${(secondTime - begin)/1000} seconds have passed`);
})();
